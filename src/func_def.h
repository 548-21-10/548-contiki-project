
/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL LOG_LEVEL_INFO

/* Configuration */
#define SEND_INTERVAL (3 * CLOCK_SECOND)


#define CLOCK_PRIORITY_HIGHEST 10
#define CLOCK_PRIORITY_HIGHER 8
#define CLOCK_PRIORITY_HIGH 6
#define CLOCK_PRIORITY_NORMAL 5
#define CLOCK_PRIORITY_LOW 4
#define CLOCK_PRIORITY_LOWER 2
#define CLOCK_PRIORITY_LOWEST 0

#define CLOCK_SOURCE_GPS 10
#define CLOCK_SOURCE_NTP 5

#define I_AM_MASTER 1
#define I_AM_NOT_MASTER 0

#define MASTER_FOUND 1
#define MASTER_NOT_FOUND 0

#define MESSAGE_TYPE_CLOCK_DEF  0x10
#define MESSAGE_TYPE_INIT       0x20
#define MESSAGE_TYPE_SYNC       0x30
#define MESSAGE_TYPE_DELAY_REQ  0x40
#define MESSAGE_TYPE_FOLLOW_UP  0x50
#define MESSAGE_TYPE_DELAY_RESP 0x60

#define MAX_CLOCK_OFFSET 1048576

/*---------------------------------------------------------------------------*/

typedef struct struct_clock_def {
    uint8_t  priority;
    uint8_t  source;
    uint16_t uuid;
} ClockDefinition;

typedef struct struct_clock_master {
    uint8_t am_i_master;
    uint8_t found_master;
    linkaddr_t clock_master_addr;
    ClockDefinition clock_master_def;
} ClockMaster;


typedef struct struct_delay_measurement {
  unsigned long t1;
  unsigned long t2;
  unsigned long t3;
  unsigned long t4;
} DelayMeasurement;

typedef struct time_message {
  uint8_t  time_identifier;
  unsigned long time_value;
} TimeMessage;

typedef struct struct_network_definition {
  uint8_t message_type;
} NetworkDefinition;

typedef struct struct_network_clock_def {
  NetworkDefinition network_definition;
  ClockDefinition clock_definition;
} NetworkClockDefinition;

typedef struct struct_network_time_message {
  NetworkDefinition network_definition;
  TimeMessage time_message;
} NetworkTimeMessage;


typedef uint32_t Generator;




static unsigned get_usec(){
  // get u seconds
  struct timeval t;
  gettimeofday(&t, NULL);

  return /*t.tv_sec * 1000000 +*/ t.tv_usec;
}


const char *str_message_type(uint8_t type){
  switch( type ){
    case MESSAGE_TYPE_CLOCK_DEF:
      return "Clock definition";
      break;
    case MESSAGE_TYPE_INIT:
      return "Init message";
      break;
    case MESSAGE_TYPE_SYNC:
      return "Sync message";
      break;
    case MESSAGE_TYPE_DELAY_REQ:
      return "Delay request message";
      break;
    case MESSAGE_TYPE_FOLLOW_UP:
      return "Follow up message";
      break;
    case MESSAGE_TYPE_DELAY_RESP:
      return "Delay response message";
      break;
    default:
      return "Unknown message type";
  }
}


void tx( const linkaddr_t *dest, void *ptr, int size ){
    LOG_INFO("TX start.\n");
    
    LOG_INFO("TX size: %d.\n", size);

    //memcpy(nullnet_buf, ptr, size);
    nullnet_buf = (uint8_t *)ptr;
    nullnet_len = size;

    LOG_INFO("Netstack output.\n");

    NETSTACK_NETWORK.output(dest);
    LOG_INFO("TX end.\n");
}

void send_data( const linkaddr_t *dest, uint8_t type, void *ptr, int size ){
  LOG_INFO("Sending %s to ", str_message_type(type) );
  LOG_INFO_LLADDR(dest);
  LOG_INFO_("\n");

  if( type == MESSAGE_TYPE_CLOCK_DEF ){
    LOG_INFO("Clock definition message start.\n");
    NetworkClockDefinition net_clock_def;
    net_clock_def.network_definition.message_type = type;
    memcpy(&net_clock_def.clock_definition, ptr, sizeof(ClockDefinition));

    tx( dest, &net_clock_def, sizeof(NetworkClockDefinition) );

    LOG_INFO("Clock definition message end.\n");
    return;
  } else if( type == MESSAGE_TYPE_INIT || 
    type == MESSAGE_TYPE_SYNC || 
    type == MESSAGE_TYPE_FOLLOW_UP || 
    type == MESSAGE_TYPE_DELAY_REQ || 
    type == MESSAGE_TYPE_DELAY_RESP
  ){
    LOG_INFO("Network timing message start.\n");

    NetworkTimeMessage net_time_msg;
    net_time_msg.network_definition.message_type = type;
    memcpy(&net_time_msg.time_message, ptr, sizeof(TimeMessage));

    tx( dest, &net_time_msg, sizeof(NetworkTimeMessage) );

    LOG_INFO("Network timing message end.\n");
    return;
  }
  
  memcpy(nullnet_buf, ptr, size);
  nullnet_len = size;

  NETSTACK_NETWORK.output(dest);
}


void trigger_time_op(uint8_t type, const linkaddr_t *dest, int offset ){
  LOG_INFO("Triggering timing operation.\n");
  TimeMessage tm;
  tm.time_identifier = type;
  tm.time_value = get_usec() + offset;
  send_data(dest, type, &tm, sizeof(TimeMessage));
}

void trigger_init( const linkaddr_t *dest, int offset  ){
  LOG_INFO("Triggering init message.\n");
  trigger_time_op( MESSAGE_TYPE_INIT, dest, offset );
}

void trigger_sync( const linkaddr_t *dest, int offset  ){
  LOG_INFO("Triggering sync message.\n");
  trigger_time_op( MESSAGE_TYPE_SYNC, dest, offset );
}

void trigger_delay_req( const linkaddr_t *dest, int offset  ){
  LOG_INFO("Triggering delay request message.\n");
  trigger_time_op( MESSAGE_TYPE_DELAY_REQ, dest, offset );
}

void trigger_delay_resp( const linkaddr_t *dest, int offset  ){
  LOG_INFO("Triggering delay response message.\n");
  trigger_time_op( MESSAGE_TYPE_DELAY_RESP, dest, offset );
}

void trigger_delay_follow_up( const linkaddr_t *dest, int offset  ){
  LOG_INFO("Triggering delay follow up message.\n");
  trigger_time_op( MESSAGE_TYPE_FOLLOW_UP, dest, offset );
}


/*-------------------------------------------------------------------------------------*/
/**
 * @brief 
 * 
 * @param clock1 
 * @param clock2 
 * @return int 0 if clock1 is best clock, 1 if clock2 is best clock, -1 on same
 */
int compare_clocks( ClockDefinition clock1, ClockDefinition clock2 ){
  if( clock1.priority > clock2.priority ){
    return 0;
  } else if( clock1.priority < clock2.priority  ){
    return 1;
  } else if( clock1.source > clock2.source ){
    return 0;
  } else if( clock1.source < clock2.source ){
    return 1;
  } else if( clock1.uuid > clock2.uuid ){
    return 0;
  } else if( clock1.uuid < clock2.uuid ){
    return 1;
  } else {
    return -1;
  }
}