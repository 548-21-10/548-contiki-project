/*
 * 
 *
 */

/**
 * \file
 *         
 * \author
*         
 *
 */

#include "contiki.h"
#include "net/netstack.h"
#include "net/nullnet/nullnet.h"


#include <string.h>
#include <stdio.h> /* For printf() */
#include <stdlib.h> 
#include <time.h>
#include <sys/time.h>

#include "func_def.h"

/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL LOG_LEVEL_INFO


#define TESTING 1




/*---------------------------------------------------------------------------*/

ClockDefinition clock_def;
ClockMaster master;
DelayMeasurement delay_measure;
int clock_offset = 0;
int has_init = 0;


void init_clock_struct( ClockDefinition *clock, int gen ){
  clock_def.priority = gen % CLOCK_PRIORITY_HIGHEST;
  clock_def.source = gen % CLOCK_SOURCE_GPS;
}

void init_clock_master( ClockMaster *master2 ){
  master.am_i_master = I_AM_NOT_MASTER;
  master.found_master = MASTER_NOT_FOUND;
  memset( &master.clock_master_addr, 0, sizeof(linkaddr_t) );
  // master->clock_master_addr = NULL; // {{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }};
}

void init_delay_measurement( DelayMeasurement *delay ){
  delay_measure.t1 = 0;
  delay_measure.t2 = 0;
  delay_measure.t3 = 0;
  delay_measure.t4 = 0;
}


void broadcast_clock_stats( ClockDefinition *clock ){
  LOG_INFO("Broadcasting clock information");
  LOG_INFO_("\n");

  send_data( NULL, MESSAGE_TYPE_CLOCK_DEF, (void *) clock, sizeof(ClockDefinition) );
  
}

/*-------------------------------------------------------------------------------------*/
void send_init_request_if_have_master(){
  
  LOG_INFO("Checking for master.\n");
  if( master.found_master == MASTER_NOT_FOUND ){
  LOG_INFO("No master.\n");
    return;
  }

  LOG_INFO("Master found, sending init request.\n");

  // only need the message init type, the master will respond with the timing requests
  TimeMessage tm;
  tm.time_identifier = MESSAGE_TYPE_INIT;
  send_data(&master.clock_master_addr, MESSAGE_TYPE_INIT, &tm, sizeof(TimeMessage));
}



void set_new_master( ClockDefinition network_clock, const linkaddr_t *src ){

  // only usurp master if new clock beats old master
  int clock_cmp = 0;
  if( master.found_master == MASTER_FOUND ){
    if( memcmp(&master.clock_master_addr, src, sizeof(linkaddr_t)) == 0 ){
      return;
    }
    LOG_INFO("Checking against old master. ");
    clock_cmp = compare_clocks(network_clock, master.clock_master_def);
  }
   
  if( clock_cmp == 0 ){
    LOG_INFO("New master: ");
    LOG_INFO_LLADDR(src);
    LOG_INFO_("\n");
    memcpy(&master.clock_master_addr, src, sizeof(linkaddr_t));
    master.found_master = MASTER_FOUND;
    return;
  }
  LOG_INFO("No new master.\n");
}


void unset_if_master( const linkaddr_t *src ){
  if( memcmp(src,&master.clock_master_addr,sizeof(linkaddr_t))==0 ){
    LOG_INFO("Unsetting current master.\n");
    //master.clock_master_addr = NULL; //{{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }};
    memset( &master.clock_master_addr, 0, sizeof(linkaddr_t) );
    master.found_master = MASTER_NOT_FOUND;
  }
}


void update_master( ClockDefinition network_clock, const linkaddr_t *network_src ){
  // compare clock_def to network_clock
  // priority, source, uuid
  int clock_cmp = compare_clocks(network_clock, clock_def);

  if( clock_cmp == 0 ){
    LOG_INFO("Network clock better than local clock.\n");
    set_new_master( network_clock, network_src );
  } else if( clock_cmp == 1  ){
    unset_if_master( network_src );
  } else {
    LOG_INFO("MASTERS COMPLETE MATCH\n");
    unset_if_master( network_src );
  }
}


/*---------------------------------------------------------------------------*/
void record_t1( unsigned long t1 ){
  LOG_INFO("Recording t1\n");
  delay_measure.t1 = t1;
}

void record_t2(){
  LOG_INFO("Recording t2\n");
  delay_measure.t2 = (unsigned long) get_usec();
}

void record_t3( unsigned long t3 ){
  LOG_INFO("Recording t3\n");
  delay_measure.t3 = t3;
}

void record_t4(){
  LOG_INFO("Recording t4\n");
  delay_measure.t4 = (unsigned long) get_usec();
}


/*---------------------------------------------------------------------------*/
void update_clock(){
  LOG_INFO("Caclulating clock offset.\n");
  int ms_difference = delay_measure.t2 - delay_measure.t1;
  int sm_difference = delay_measure.t4 - delay_measure.t3;


  LOG_INFO("Registered times:\n\tt1: %lu\n\tt2: %lu\n\tt3: %lu\n\tt4: %lu\n", delay_measure.t1, delay_measure.t2, delay_measure.t3, delay_measure.t4);

  // assume delay is the same each way
  int offset = (ms_difference - sm_difference)/2;

  if( offset != 0 ){
    int old_clock_offset = clock_offset;
    clock_offset = offset;
    LOG_INFO("UPDATING clock offset from %d to %d.\n", old_clock_offset, clock_offset);
  } else {
    LOG_INFO("No clock offset update.\n");
  }
}


/*---------------------------------------------------------------------------*/
PROCESS(ieee_1588_master, "IEEE 1588 implementation");
AUTOSTART_PROCESSES(&ieee_1588_master);

/*---------------------------------------------------------------------------*/
void input_callback(const void *data, uint16_t len,
  const linkaddr_t *src, const linkaddr_t *dest)
{
  // check that size of len is at least one byte
  // read one byte to detirmine type
  // check that len is not longer than expected type size+1
  // read in type size to data
  // process data accordingly

  if( len < sizeof(NetworkDefinition) ){
    LOG_INFO("Bad message recieved;");
    return;
  }

  NetworkDefinition net_def;
  memcpy(&net_def, data, sizeof(NetworkDefinition));

  switch(net_def.message_type){
    case MESSAGE_TYPE_CLOCK_DEF:
      LOG_INFO("Clock definition recieved\n");

      // check data size
      if( len < sizeof(NetworkClockDefinition) ){
        return;
      }

      NetworkClockDefinition net_clock_def;
      memcpy(&net_clock_def, data, sizeof(NetworkClockDefinition));
      
      update_master( net_clock_def.clock_definition, src );

      break;
    case MESSAGE_TYPE_INIT:
      LOG_INFO("Time synch init recieved\n");

      // trigger sync, ignore rest of data
      trigger_sync( src, 0 );

      break;
    case MESSAGE_TYPE_SYNC:
      LOG_INFO("Time synch recieved\n");
      record_t2();

      
      if( len < sizeof(NetworkTimeMessage) ){
        return;
      }

      NetworkTimeMessage net_time_msg;
      memcpy( &net_time_msg, data, sizeof(NetworkTimeMessage) );
      record_t1( net_time_msg.time_message.time_value );

      // after sync, send delay req
      trigger_delay_req( src, 0 );

      break;
    case MESSAGE_TYPE_DELAY_REQ:
      LOG_INFO("Delay request recieved\n");

      trigger_delay_resp( src, 0 );

      break;
    case MESSAGE_TYPE_DELAY_RESP:
      LOG_INFO("Delay response recieved\n");
      record_t4();

      
      if( len < sizeof(NetworkTimeMessage) ){
        return;
      }

      NetworkTimeMessage net_time_msg2;
      memcpy( &net_time_msg2, data, sizeof(NetworkTimeMessage) );
      record_t3( net_time_msg2.time_message.time_value );

      update_clock();

      break;
    case MESSAGE_TYPE_FOLLOW_UP:
      LOG_INFO("Follow up recieved\n");
      break;
    default:
      LOG_INFO("Unkown message recieved\n");
  }
}


/*---------------------------------------------------------------------------*/
PROCESS_THREAD(ieee_1588_master, ev, data)
{
  static struct etimer periodic_timer;
  static unsigned count = 0;

  if( count == 0 ){
    srand( time(NULL) );
    const int gen = rand();
    clock_offset = gen % MAX_CLOCK_OFFSET;
    // init clock
    init_clock_struct( &clock_def, gen );

    // init clock master struct
    init_clock_master( &master );

    // init delay measurement
    init_delay_measurement( &delay_measure );
    has_init = 0;
  }
  

  PROCESS_BEGIN();

  /* Initialize NullNet */
  nullnet_buf = (uint8_t *)&clock_def;
  nullnet_len = sizeof(ClockDefinition);
  nullnet_set_input_callback(input_callback);



  etimer_set(&periodic_timer, SEND_INTERVAL);
  while(1) {
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&periodic_timer));

    int clock_off = clock_offset;
    LOG_INFO("CLOCK OFFSET %u. \n", clock_off);

    if( count % 2 == 0 ){
      broadcast_clock_stats(&clock_def);
    } else {
      send_init_request_if_have_master();
    }
    
    count++;
    etimer_reset(&periodic_timer);
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
